import JSDOM from 'jsdom-global';
import { addPoints } from '../utils.js';
import { getQuadtreeDist } from '../object-groups.js';

import {
    testPoints2,
    distOutput2,
    distOutput2_new
} from '../test-data.js';

import Quadtree from '../quadtree-extended.js';

const qTreeSampleRadius = 100;
let distLoopCount = 0;

const pointRadius = 17;
const threshold = 40;
const qTreeMaxItemsPerNode = 4;
const qTreeMaxDepth = 4;
const qtParams = {};
const sampleRadius = 100;
qtParams.qTreeSampleRadius = 100;

const qTree = new Quadtree(
  {
      x: 0,
      y: 0,
      width: 800,
      height: 800
  },
  qTreeMaxItemsPerNode, 
  qTreeMaxDepth
);

let testPoints = [
  {
      "x": 44,
      "y": 92,
      "index": 0,
      "selected": false
  },
  {
      "x": 148,
      "y": 179,
      "index": 1,
      "selected": false
  },
  {
      "x": 94,
      "y": 136,
      "index": 2,
      "selected": false
  }
]

let outputVal = [
  [1, 2],
  [2, 0]
]

// old algo before filtering of reverse pairs: eg [0,1] == [1,0] as they are the same two points
// let outputValLoaded = [
//   [0,2],
//   [1,2],
//   [2,0],
//   [2,1]
// ]

let outputValLoaded = [
  [0,2],
  [1,2]  
]

let points = addPoints(testPoints,qTree,pointRadius);
let inputVal = getQuadtreeDist(points,qTree,sampleRadius,threshold).near;

test('gets distances using a quadtree', () => {
  // document.body.innerHTML += `  
  //     <li><strong>qTreeMaxDepth [ <input id="qTreeMaxDepthElm" type="numeric" value="7" /> ]: </strong>max qTree node depth</li>
  //     <li><strong>qTreeMaxItemsPerNode [ <span id="qTreeMaxItemsPerNodeElm">0</span> ]: </strong>Number of items in each node before the tree subdivides into the next 4 quads.</li>    
  // `;

  // let qTreeMaxDepthVal = document.getElementById('qTreeMaxDepthElm').value;
  // let qTreeMaxItemsPerNodeVal = document.getElementById('qTreeMaxItemsPerNodeElm');

  // console.log('testPoints2: ', testPoints2);
  console.log('inputVal: ', inputVal);
  // console.log('inputData: ', getQuadtreeDist(testPoints2));
  // console.log('outputData: ', distOutput2);

  const dom = new JSDOM(
    `
    <html>
       <body>
        <canvas id="canvas"></canvas>
        <li><strong>qTreeMaxDepth [ <input id="qTreeMaxDepthElm" type="numeric" value="7" /> ]: </strong>max qTree node depth</li>
        <li><strong>qTreeMaxItemsPerNode [ <span id="qTreeMaxItemsPerNodeElm">0</span> ]: </strong>Number of items in each node before the tree subdivides into the next 4 quads.</li>
      </body>
     </html>    
  `,
  { url: 'http://localhost' }
  );

  // console.log('testPoints2: ', testPoints2);
  // console.log('inp: ', inputVal); 
  // console.log('out: ', outputValLoaded);
 
  expect(inputVal).toEqual(outputValLoaded);
});
