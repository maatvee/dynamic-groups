// ui.js

//import { DG } from './main.js';

// (async () => {
//     if (window !== 'undefined') {
//       const DG = await import('./main.js');
//     }
// })();

import { Circle, Line, Quadtree, Rectangle} from './quadtree-beta-esm.js';

import { DG, exampleOptions } from './main.js';

import {
    drawGrid,
    getHoverPoint,
    getHoverPointQuad,
    update,
    debounce,
    runGroups,
    quadTreeDraw,
    addRandomPoints,
    getBoundingBox,
    addPoints,
    movePoint,
    query
} from './utils.js';

function initApp(qtParams){
    //dynamic groups in the global scope
    if (typeof window !== 'undefined') {
        window.DG = DG; // so we can debug what is happening with DG in the console
    }    

    DG.canvas.width = DG.ui.canvasWidth;
    DG.canvas.height = DG.ui.canvasHeight;
    
    if (typeof DG.qTreeMaxDepthVal !== 'undefined') {
        DG.qTreeMaxDepthVal.innerHTML = DG.qtParams.qTreeMaxDepth;
    }

    if (DG.qTreeMaxItemsPerNodeVal){
        DG.qTreeMaxItemsPerNodeVal.innerHTML = DG.qtParams.qTreeMaxItemsPerNode;
    }

    DG.thresholdRangeElm.addEventListener('input',() => {

        let threshold = document.getElementById('thresholdRange').value;
        DG.threshold = threshold;
        threshold = parseInt(threshold,10);

        DG.rangeValElm.innerHTML = threshold;
        update(DG.ctx);    
    });

    DG.thresholdRangeElm.addEventListener('change',() => {

        runGroups();
    });

    DG.addPointsButton.addEventListener('click', evt => {
        addRandomPoints(1000);
    });

    DG.loadExampleSelect.addEventListener('change', evt => {
        DG.pointArray = [];
        DG.qTree.clear();
        
        let opt = evt.target.value;
        DG.pointArray = addPoints(exampleOptions[opt],DG.qTree,DG.threshold);
        update(DG.ctx);
        quadTreeDraw(DG.ctx,DG.qTree);
    });

    DG.canvas.addEventListener('mousemove', evt => {
        
        DG.canvas.mouseX = evt.offsetX;
        DG.canvas.mouseY = evt.offsetY;
        
        if (DG.canvas.mouseHover && DG.canvas.mouseDown) {
            movePoint(DG.canvas.activePoint,DG.canvas.mouseX,DG.canvas.mouseY);        
        }

        getHoverPoint(DG.pointArray);
        //getHoverPointQuad(DG.pointArray);
        update(DG.ctx);
        // qTree.draw(ctx);    
        quadTreeDraw(DG.ctx,DG.qTree);

        DG.output.innerHTML = `x:${DG.canvas.mouseX} y:${DG.canvas.mouseY}`;
    });

    //disables context menu (right click)
    DG.canvas.addEventListener('contextmenu', e => {
        e.preventDefault();
    });

    DG.canvas.addEventListener('mousedown', evt => {
        // show qTree sample on right click
        if (evt.button == 2) {
            
            DG.canvas.mouseHover = false;
            // qTree.draw(ctx);
            
            const rad = DG.qtParams.qTreeSampleRadius;
            const bounds = {
                x: DG.canvas.mouseX,
                y: DG.canvas.mouseY,
                r: rad
            }

            query(bounds);
            quadTreeDraw(DG.ctx,DG.qTree);

        } else {
            DG.canvas.mouseDown = true;        
        }

        if (!DG.canvas.mouseHover && DG.canvas.mouseDown) {

            let pointObject = {
                x: evt.offsetX,
                y: evt.offsetY,
                index: DG.pointArray.length,
                selected: false
            };

            const circle = new Circle({
                x: pointObject.x,
                y: pointObject.y,
                r: DG.ui.pointRadius,
                data: {
                    index: DG.pointArray.length
                }
            });
            
            DG.qTree.insert(circle);

            DG.pointArray.push(pointObject);        

            DG.canvas.mouseHover = true;
            //getHoverPoint(DG.pointArray);
            //getHoverPointQuad(DG.pointArray);
            update(DG.ctx);
        }    
    });

    DG.canvas.addEventListener('mouseup', evt => {
        DG.canvas.mouseDown = false;

        // re-init the quadtree when the mouse is released
        // ie after points get moved etc.
        // we have to copy the existing points array, then reset it and re-add the copied points

        DG.qTree.clear();
        let newPoints = [...DG.pointArray];
        DG.pointArray = [];
        DG.pointArray = addPoints(newPoints,DG.qTree,DG.threshold);
    });

    // draws the initial grid background of the canvas
    // needs to be called after all the inital setup
    drawGrid(DG.ctx);

} // end initApp

export { initApp }