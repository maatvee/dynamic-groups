// main.js

/**
 * dynamic-groups
 * @version 0.0.1
 * @license MIT
 * @author Mat Valdman
 */

/* https://gitlab.com/maatvee/dynamic-groups */
 
/*
Copyright © 2020-2022 Mat Valdman

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// quadTreeDraw(ctx,qTree) draws the quadtree recursively to illustrate it visually

//import Quadtree from './quadtree-extended.js';
import { Circle, Line, Quadtree, Rectangle} from './quadtree-beta-esm.js';
import { getQuadtreeDist, getGroups } from './object-groups.js';
import {
    testPoints1,
    testPoints2,
    testPoints3,
    testPoints4,
    testPoints5,
    testPoints6,
    testPoints7,
    testPoints8,
    testPoints9
} from './test-data.js';

import {
    initApp,
} from './ui.js';

const DG = {}; 

DG.qtParams = {
    qTreeSampleRadius: 40,
    qTreeMaxDepth: 4,
    qTreeMaxItemsPerNode: 4
}

// Data
DG.pointArray = [];
DG.distanceArray = [];
DG.groupCount = 0;
//console.log('qtParams: ', qtParams);

// relevant to object groups
DG.threshold = 40;
// let distLoopCount = 0;
//let finalArr = [];
DG.distLoopCount = 1;
DG.loopCount = 0;

DG.outputArr = [];
DG.runCount = 0;
DG.runCountMax = 1000; // short circuit for recursive loop - just in case anything goes wrong with the termination check
//let count = 0;
// UI Params
DG.ui = {
    decPlaces: 3,
    canvasWidth: 800,
    canvasHeight: 800,
    gridSize: 40,
    get gridWidth() { return this.canvasWidth / this.gridSize },
    get gridHeight() { return  this.canvasHeight / this.gridSize },
    get offsetH() { return  this.canvasWidth / 2 - 1 },
    get offsetV() { return  this.canvasHeight / 2 - 1 },
    baseFont: "Helvetica Neue",
    baseFontSize: "12px",
    pointRadius: 17
}
//if (typeof document !== 'undefined') {
    // ui elements
    DG.canvas = document.getElementById('canvas');
    DG.output = document.getElementById('output');
    DG.addPointsButton = document.getElementById('addPointsButtonElm');
    DG.loadExampleSelect = document.getElementById('loadExample');
    DG.thresholdRangeElm = document.getElementById('thresholdRange');
    DG.rangeValElm = document.getElementById('rangeVal');
    DG.qTreeMaxDepthVal = document.getElementById('qTreeMaxDepthElm');
    DG.qTreeMaxItemsPerNodeVal = document.getElementById('qTreeMaxItemsPerNodeElm');
    DG.groupsCounterElm = document.getElementById('groupsCounter');
    DG.pointsCounterElm = document.getElementById('pointsCounter');
    DG.ctx = DG.canvas ? DG.canvas.getContext('2d') : '';

    DG.canvas.activePoint = -1;

//}

const exampleOptions = [
    testPoints1,
    testPoints2,
    testPoints3,
    testPoints4,
    testPoints5,
    testPoints6,
    testPoints7,
    testPoints8,
    testPoints9
]

// init basic quadtree
let params = {
    get width() { return DG.ui.canvasWidth },
    get height() { return DG.ui.canvasHeight },
    x: 0,           // optional, default:  0
    y: 0,           // optional, default:  0
    maxObjects: DG.qtParams.qTreeMaxItemsPerNode, // optional, default: 10
    maxLevels: DG.qtParams.qTreeMaxDepth    // optional, default:  4
}

DG.qTree = new Quadtree(params);

// DG.qTree = new Quadtree(
//     {
//         x: 0,
//         y: 0,
//         get width() { return DG.ui.canvasWidth },
//         get height() { return DG.ui.canvasHeight }
//         // width: ui.canvasWidth,
//         // height: ui.canvasHeight
//     },
//     DG.qtParams.qTreeMaxItemsPerNode, 
//     DG.qtParams.qTreeMaxDepth
// );

// console.log('qTreeMaxDepthVal:', qTreeMaxDepthVal);

initApp(DG.qtParams);
//console.log('uiElms:', uiElms);

//drawGrid(); called from within initApp()

export { 
    DG,
    //qTree,
    //ui,
    //qtParams,
    //threshold,
    // //finalArr,
    // //runCount,
    //runCountMax,
    exampleOptions,
    //pointArray
    // getDistance,
    // getDistanceSqr,    
    //qTreeSampleRadius
    
    //count
}

/*
References to similar problems and their solutions

https://stackoverflow.com/questions/480316/how-do-i-group-objects-in-a-set-by-proximity
https://en.wikipedia.org/wiki/K-means_clustering
https://en.wikipedia.org/wiki/Weber_problem
http://www.mqasem.net/vectorquantization/vq.html
https://stackoverflow.com/questions/23668456/clustering-objects-with-a-distance-matrix
*/