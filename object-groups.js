// object-groups.js

//import { DG } from './main.js';
import { Circle, Line, Quadtree, Rectangle} from './quadtree-beta-esm.js';

(async () => {
    if (typeof window !== 'undefined') {
      const DG = await import('./main.js');
    }
})();

import {
    getDistance,
    getDistanceSqr,
    uPush
} from './utils.js';

// local vars
let count = 0;
// let distLoopCount = 1;
// let loopCount = 0;

// this should be significantly more performant than the original getDist
// OlogN vs ON^2
// uses a quadtree to find the nearest points to the current sample point, rather than checking every point
function getQuadtreeDist(distArr,qTree,sampleRadius,threshold) {
    let result = [];
    let tempResult = new Set();
    let itemArr = [];
    let farItemArr = [];
    let farIndices = [];
    let nearInd = [];
    let farInd = [];
    let allInd = [];
    //let count = 0;

    //let distLoopCount = DG.distLoopCount;
    //let loopCount = DG.loopCount;
     
    let pointMap = new Map();
    //console.log('distLoopCount:', distLoopCount);
    //console.log('getQuadtreeDist param:', {distArr,qTree,sampleRadius,threshold});

    for (let o = 0; o < distArr.length; o++ ) {

        const searchCircle = new Circle({
            x: distArr[o].x,
            y: distArr[o].y,
            r: sampleRadius
        });

        var elements = qTree.retrieve(searchCircle);
        
        if (elements.length > 0) {

            for (let i = 0; i < elements.length; i++ ) {
                let point1 = {
                    x: distArr[o].x,
                    y: distArr[o].y,
                }
                let point2 = {
                    x: elements[i].x,
                    y: elements[i].y,
                }

                let mapKey = `${o},${elements[i].data.index}`;
                let mapKeyR = `${elements[i].data.index},${o}`;

                // let mapKey = [o,elements[i].data.index];
                // let mapKeyR = [elements[i].data.index,o];

                if (pointMap.has(mapKey) || pointMap.has(mapKeyR)){
                    continue;                    
                    //console.log('pointMap:', pointMap);
                } 
                
                if ( o !== elements[i].data.index ) {     
                    pointMap.set(mapKey,'');
                    pointMap.set(mapKeyR,'');  
                    //console.log('loopCount:', loopCount);             

                    let distance = getDistance(point1,point2);
                    //let distanceSqr = getDistanceSqr(point1,point2);                                
                    //loopCount++
                    //var adjustedThreshold = threshold * 2;
                    //if (distanceSqr <= (adjustedThreshold * adjustedThreshold)) {
                    if (distance <= threshold * 2) {
                        result.push([o,elements[i].data.index]);
                        itemArr.push([distArr[o],elements[i].data.index]);
                        tempResult.add(o);
                        tempResult.add(elements[i].data.index);
                        //uPush(nearInd,o);
                        //uPush(nearInd,elements[i].data.index);
                    } else {
                        farItemArr.push([distArr[o],elements[i].data.index]);
                        farIndices.push([o,elements[i].data.index]);
                    }
                }
                
                
                //distLoopCount++;
            }
        }        
    }
    // here we convert the set into an array
    nearInd = [...tempResult];

    // here we create the farInd (far indexes) by pushing the missing indexes into the farInd array
    for (let i = 0; i < distArr.length; i++ ) {

        //if (nearInd.indexOf(i) == -1) {
        if (!tempResult.has(i)) {
            farInd.push([i]);
            //result.push([i]); // also include them in the result output for single point groups
        }  
    }

    // let outputObject = {
    //     near: result,
    //     far: farInd
    // }

    result = result.filter( el => el.length > 0);

    return {
        near: result,
        far: farInd
    }
}


// more than 150 points causes serious issues, since every point need to be compared with every other to find the distances between them.
const getDist = (distArr,threshold) => {
    let result = [];
    let itemArr = [];
    let farItemArr = [];
    let farIndices = [];
    let nearInd = [];
    let farInd = [];
    let allInd = [];
    let count = 1;

    for (let o = 0; o < distArr.length; o++ ) {
        for (let i = 0; i < o; i++ ) {

            let point1 = {
                x: distArr[o].x,
                y: distArr[o].y,
            }
            let point2 = {
                x: distArr[i].x,
                y: distArr[i].y,
            }

            let distance = getDistance(point1,point2);

            if (distance / 2 <= threshold) {
                result.push([o,i]);
                itemArr.push([distArr[o],distArr[i]]);
                uPush(nearInd,o);
                uPush(nearInd,i);
            } else {
                farItemArr.push([distArr[o],distArr[i]]);
                farIndices.push([o,i]);
            }
            
            //distLoopCount++;

        }
    }

    // here we create the farInd (far indexes) by pushing the missing indexes into the farInd array
    for (let i = 0; i < distArr.length; i++ ) {

        if (nearInd.indexOf(i) == -1) {
            farInd.push(i);
            result.push([i]); // also include them in the result output for single point groups
        }  
    }
    return result.filter( el => el.length > 0);
}

// function fillArr(num){
//     let arr = [];
//     for (let i=0; i < num; i++) {
//         arr[i] = [];
//     }
//     return arr
// }

const testAB = (set,nextSet,tArr) => {
    let tempTest = false; // flag to indicate a match
    // copy the arrays to break the reference to the originals
    let setA = [...set];
    let setB = [...nextSet];

    for (let o = 0; o < setA.length; o++){
        for (let i = 0; i < setB.length; i++){
            
            if (setB[i] === setA[o]) {
                tempTest = true;

                // as soon as we find a match, we shove all the unique elements into tArr and break out of the loop;
                // this is also why we have to pass in tArr to the test function, so that it stays persistent
                for (let s2 = 0; s2 < setB.length; s2++){                      
                    uPush(tArr,setB[s2]);
                }                                       
                continue;
            }            
        }
    };
    // returning an empty array instead of false doesn't work here
    return tempTest ? tArr : false; 
}

// const inArrTest = (elm,arr) => {
//     return arr.indexOf(elm) == -1;
// } 
//let runCount = 0;

// since getGroups is called recursively we need to pass in finalArr each subsequent time we call it except the first time
function getGroups(testData,finalArr = []){  

    let runCount = 0;

    if (!testData.length) {
        //console.error('Empty Array Probably means that all the objects are too far away from each other and should all be in their own separate groups');
        return [];
    }

    if (runCount > DG.runCountMax){ // stop recursive looping forever
        console.warn('max runCount exceeded, terminating:', runCount);
        return [];
    }

    const arrLen = testData.length; 
    let outGroup = []; //fillArr(arrLen);
    let noArr = []; //fillArr(arrLen);     
    let temp;     
    let tArr = [];             
        
    // here we put all of set into the tArr first
    // we need to pass in tArr into the testAB() function as it is called multiple times in the loop
    // it can't be initialised each time from inside the test function

    tArr = testData && testData[0] && testData[0].length ? [...testData[0]] : []; 

    // loop over each other element starting at the index [1]     
    // the first element is at position [0] which all the others are compared to
    for (let i = 1; i < arrLen; i++){        
        
        // test could be an array or false
        let test = testAB(testData[0],testData[i],tArr);

        if (test){
                        
            // make sure outGroup[0] is a unique array each time if test is true    
            // need to assign the test using outGroup[0], not outGroup.push()
            // for some reason using push doesn't give you the correct output        
            outGroup[0] = [...testAB(testData[0],testData[i],tArr)];            
            temp = [...outGroup[0]];

        } else {                
            // if No Match            
            noArr[i] = [...testData[i]];            
        }            
    }
    // here we assign the temp value to the first position of the noArr 
    // noArr contains the items with no matches found with each of these items
    // if it doesn't exist (ie no matches at all were found), then an emtpy array is assigned instead
    noArr[0] = temp && temp.length ? [...temp] : [];

    // removes any empty arrays from outGroup    
    let cleanOutput = outGroup.filter(el => el.length > 0);
    let groupsRemain = noArr.filter(el => el.length > 0);

    if (cleanOutput.length < 1) {
        // if there is no output, the we add the first element of the current test to the finalArr
        // there are no more matching indices to add
        finalArr.push([...testData[0]]);
    }
        
   if (groupsRemain.length > 0) {           
        
        var cleanOutputFinal = [...groupsRemain];  
        
        // ...getGroups spread operator is very important here
        // so finalArr will be a copy not a reference 
        // since the getGroups function is recursive
        // and finalArr gets passed back in each time
        finalArr = [...getGroups(cleanOutputFinal,finalArr)];
        count++;      
    }

    runCount++; // count the number of times the getGroups function is called recursively, in case we need to terminate it.

    return finalArr;
}


// keefs new version of the grouping algo
const _getGroups = (input) => {

    // console.log(input)

    // to turn the input of pairs of numbers into groups
    // we need to create a graph and then traverse it, 
    // spitting out lists of connected numbers

    // create a bunch of edges [fromPoint,toPoint]
    // do both directions to make things easier later
    const edges = []
    input.forEach(input => {
        const p1 = input[0]
        const p2 = input.length === 1 ? input[0] : input[1]
        const edge = [Math.min(p1, p2), Math.max(p1, p2)]
        edges.push(edge)
        edges.push([edge[1], edge[0]])
    })

    // console.log(edges)

    // now make a map (dictionary) of each edge so we can look up which points point to which other points
    const pointConnectionsMap = new Map() // point->Set<point>
    edges.forEach(e => {
        const existingEdges = pointConnectionsMap.get(e[0]) || new Set()
        existingEdges.add(e[1])
        pointConnectionsMap.set(e[0], existingEdges)
    })

    // console.log(pointConnectionsMap)

    // then traverse this graph and create an array of each path
    // yes this is recursive but still O(n) pretty much, since
    // we remember where we've visited so we only visit each point once

    const visited = new Set() // points
    const setPath = (p, path) => {
        if (!visited.has(p)) {
            visited.add(p)
            path.push(p)
            const nextPoints = pointConnectionsMap.get(p) || []
            nextPoints.forEach(nextP => {
                setPath(nextP, path)
            })
        }
    }

    const points = Array.from(pointConnectionsMap.keys())

    const resultPaths = [] // point[][]
    points.forEach(p => {
        const path = []
        setPath(p, path)
        if (path.length > 0) resultPaths.push(path)
    })

    return resultPaths
}

if (typeof window !== 'undefined') {
    window.getQuadtreeDist = getQuadtreeDist;
}


export {
    //uPush,
    getQuadtreeDist,
    getDist,
    testAB,
    getGroups,
    _getGroups
}