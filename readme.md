# Dynamic Grouping using a Quadtree to improve performance
Uses a QuadTree to Dynamically group points, based on a common distance threshold radius

<img src="quadtree-example.png" height="300" />


## Installation

Node version: 16.13.1

`$ npm install`

### Example Page:
https://maatvee.gitlab.io/dynamic-groups/

### Keef vs Mat Benchmark Results
https://jsben.ch/6NHBd

### References:
https://github.com/timohausmann/quadtree-js/

