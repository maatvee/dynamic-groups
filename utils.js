// utils.js

import { Circle, Line, Quadtree, Rectangle} from './quadtree-beta-esm.js';

(async () => {
    if (typeof window !== 'undefined') {
      const DG = await import('./main.js');
    }
})();

import {
    getGroups,
    _getGroups,
    getDist,
    getQuadtreeDist
} from './object-groups.js';

// Constants
const CHANNELS_PER_PIXEL = 4; //rgba
const NEG_INF = Number.NEGATIVE_INFINITY;
const POS_INF = Number.POSITIVE_INFINITY;

let runCount = 0;

/*
* uPush()
* ------------------------------------------------------------------------------------
* unique push, only add an element if it doesn't exist in the array.
* uPush(array, element)
* this version mutates the provided array
*/
const uPush = (arr,elm) => {
    if (arr.indexOf(elm) == -1) {
        arr.push(elm);
    }
}

function uniqueArray(a) {
    return [...new Set(a.map(o => JSON.stringify(o)))].map(s => JSON.parse(s));
}

function randInt(maxVal) {
    return Math.floor(Math.random() * maxVal);
}

function movePoint(id,x,y){
    if (id === -1) return
    DG.pointArray[id].x = x;
    DG.pointArray[id].y = y;
}

// Example Usage: 
// sampleObject = {x:100,y:100,width:200,height:200}
// number of points to randomly create
// numPoints = 20
// query(numPoints,sampleObject)

function query(searchBounds) {
    const ctx = DG.ctx;
    const qTree = DG.qTree;
    const threshold = DG.threshold;
    const queryCircle = new Circle({
        x: searchBounds.x,
        y: searchBounds.y,
        r: threshold//searchBounds.r 
    });

    const elements = qTree.retrieve(queryCircle);
    let x = searchBounds.x;
    let y = searchBounds.y;
    let w = searchBounds.width;
    let h = searchBounds.height;

    // qTree.draw(ctx);
    quadTreeDraw(ctx,qTree);
    
    elements.map((point,index) => {
        basicCircle(ctx, point.x, point.y, threshold, '', 'black', 2);
    });

    // draw query bounds
    ctx.strokeStyle = 'blue';    
    basicCircle(ctx, x, y, threshold, '', 'blue', 2);
}


function addPoints(data,qTree,pointRadius,startIndex = 0){

    let output = [];

    if (!data || data.length < 1){
        console.warn('No data supplied as argument in addPoints')
        return output;
    }
    
    let numberOfPoints = data.length;

    for (let i = 0; i < numberOfPoints; i++ ) {
        let pointObject = {
            x: data[i].x,
            y: data[i].y,
            index: startIndex + i,
            selected: false
        };

        const circle = new Circle({
            x: pointObject.x,
            y: pointObject.y,
            r: pointRadius,
            data: {
                index: startIndex + i
            }
        });
        
        qTree.insert(circle);       
       output.push(pointObject);
    }

    return output;
}

function addRandomPoints(numberOfPoints = 10) {

    //let ctx = DG.ctx;
    let ui = DG.ui;
    let width = ui.canvasWidth;
    let height = ui.canvasHeight;
    let pointArray = DG.pointArray;

    // if (numberOfPoints > 150) {
    //     console.warn('Max number is 150');
    //     numberOfPoints = 150;
    // }

    for (let i = 0; i < numberOfPoints; i++ ) {
        let pointObject = {
            x: randInt(width),
            y: randInt(height),
            index: pointArray.length,
            selected: false
        };

        const circle = new Circle({
            x: pointObject.x,
            y: pointObject.y,
            r: ui.pointRadius,
            data: {
                index: DG.pointArray.length // here we use the pointArray length so we can append new points
            }
        });
        
        DG.qTree.insert(circle);

        DG.pointArray.push(pointObject);
    }

    update(DG.ctx);
    //DG.qTree.draw(DG.ctx);
    quadTreeDraw(DG.ctx,DG.qTree);    
}

function getDistance(p1,p2){
    return Math.sqrt(((p1.x - p2.x) * (p1.x - p2.x)) + ((p1.y - p2.y) * (p1.y - p2.y)));
}

function getDistanceSqr(p1,p2){
    return ((p1.x - p2.x) * (p1.x - p2.x)) + ((p1.y - p2.y) * (p1.y - p2.y));
}

function getHoverPoint(pointArray){
    canvas.mouseHover = false;

    for (let i = 0; i < pointArray.length; i++) {
        let point1 = {
            x: pointArray[i].x,
            y: pointArray[i].y,
        }

        let point2 = {
            x: canvas.mouseX,
            y: canvas.mouseY
        }

        let distanceSqr = getDistanceSqr(point1,point2);

        if (distanceSqr < DG.ui.pointRadius * DG.ui.pointRadius) {
            DG.pointArray[i].selected = true;            
            canvas.activePoint = i;
            canvas.mouseHover = true;
        } else {
            DG.pointArray[i].selected = false;
            canvas.style.cursor = "default";
            canvas.mouseHover = false;
            canvas.activePoint = -1;
        }
    }
}

function getHoverPointQuad(pointArray){
    canvas.mouseHover = false;
    
    const ui = DG.ui;
    const qTree = DG.qTree;
    const pointRadius = ui.pointRadius;
    //const pointArray = DG.pointArray;

    const searchCircle = new Circle({
        x: canvas.mouseX,
        y: canvas.mouseY,
        r: ui.pointRadius
    });

    let elements = qTree.retrieve(searchCircle);

    if (elements.length == 0) {
        return
    }

    for (let i = 0; i < elements.length; i++) {
        let point1 = {
            x: elements[i].x,
            y: elements[i].y,
        }

        let point2 = {
            x: canvas.mouseX,
            y: canvas.mouseY
        }

        let currIdx = elements[i].data.index;

        let distance = getDistance(point1,point2);

        

        if (distance < pointRadius) {
            console.log();
            pointArray[currIdx].selected = true;
            canvas.activePoint = currIdx;
            canvas.mouseHover = true;
        } else {
            pointArray[currIdx].selected = false;
            canvas.style.cursor = "default";
            canvas.mouseHover = false;
            canvas.activePoint = -1;
        }
    }
}

function drawPoint(x, y, id, pointColour = 'red', pointBodyColour = '') {
    let ctx = DG.ctx;
    let ui = DG.ui;

    let threshold = document.getElementById('thresholdRange').value;
    threshold = parseInt(threshold,10);
    basicCircle(ctx, x, y, threshold, '', 'rgba(0,0,255,0.3)', 1);
    basicCircle(ctx, x, y, ui.pointRadius, pointBodyColour, pointColour, 1);
    setText(id, x, y + 5, '16px', pointColour);    
}

function clearCanvas(ctx) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawGrid(ctx);
}

function debounce(fn, delay) {
    //console.log('debounce called');
    var timer = null;
    return function () {
      var context = this, args = arguments;
      //console.log('args:',args);
      clearTimeout(timer);
      //window.cancelAnimationFrame(timer);
      timer = setTimeout(function () {          
        fn.apply(context, args);
      }, delay);
    };
}

function debounceAnim(fn, delay) {
    //console.log('debounce called');
    var timer = null;
    return function () {
      var context = this, args = arguments;
      //console.log('args:',args);
      //clearTimeout(timer);
      window.cancelAnimationFrame(timer);
      timer = window.requestAnimationFrame(function () {          
        fn.apply(context, args);
      });
    };
}

// debounce limits how often runGroups runs
// a value of 100ms is a good starting point
const debounceRunGroups = debounceAnim(runGroups,50);
//const debounceRunGroups = debounce(runGroups,50);

//const debounceRunGroups = runGroups;

function update(ctx) {
    //console.log('update called');

    clearCanvas(ctx);
    
    for (let i = 0; i < DG.pointArray.length; i++) {
        let _x = DG.pointArray[i].x;
        let _y = DG.pointArray[i].y;
        let _id = i;
        let bc,col;

        if (DG.pointArray[i].selected) {
            bc = 'red';
            col = 'white';
            DG.canvas.style.cursor = "pointer";
            DG.canvas.mouseHover = true;
            DG.canvas.activePoint = i;
        } else {
            bc = 'white';
            col = 'red';            
        }
    
        drawPoint(_x, _y, _id,col,bc);           
    }

    //debounce(runGroups,200);
    //runGroups();
    debounceRunGroups();
    
}

function getBoundingBox(pointArray, offset = 0, strokeStyle = 'lime') {
    let ctx = DG.ctx;
    let ui = DG.ui;
    
    let minX = POS_INF;
    let minY = POS_INF;
    let maxX = NEG_INF;
    let maxY = NEG_INF;

    for (let i = 0; i < pointArray.length; i++) {

        let currMinX = pointArray[i].x - ui.pointRadius;
        let currMaX = pointArray[i].x + ui.pointRadius;
        let currMinY = pointArray[i].y - ui.pointRadius;
        let currMaxY = pointArray[i].y + ui.pointRadius;

        minX = minX < currMinX ? minX : currMinX;
        maxX = maxX > currMaX ? maxX : currMaX;
        minY = minY < currMinY ? minY : currMinY;
        maxY = maxY > currMaxY ? maxY : currMaxY;
    }

    // let offset = 2;

    let bounds = {
        left: minX - offset,
        right: maxX - minX + (offset * 2),
        top: minY - offset,
        bottom: maxY - minY + (offset * 2)
    }

    // draw the bounding box
    ctx.lineWidth = 1;
    //ctx.strokeStyle = 'rgba(0, 255, 0, 1)';
    //ctx.strokeStyle = 'rgb(0, 255, 0)';
    ctx.strokeStyle = strokeStyle; //'rgba(0, 255, 0, 1)';
    ctx.strokeRect(bounds.left, bounds.top, bounds.right, bounds.bottom);
}

function setpixelated(context) {
    context['mozImageSmoothingEnabled'] = false;    /* Firefox */
    context['oImageSmoothingEnabled'] = false;      /* Opera */
    context['webkitImageSmoothingEnabled'] = false; /* Safari */
    context['msImageSmoothingEnabled'] = false;     /* IE */
    context['imageSmoothingEnabled'] = false;       /* standard */
}

function setText(string, x, y, baseFontSize = '12px', textColour = 'black') {
    let ctx = DG.ctx;
    let ui = DG.ui;
    ctx.font = ui.baseFontSize + ' ' + ui.baseFont;
    //ctx.strokeStyle = 'white';
    //ctx.lineWidth = 4;
    //ctx.strokeText(string, x, y);
    ctx.fillStyle = textColour;
    ctx.textAlign = 'center';
    ctx.fillText(string, x, y);
}

function setAxisLabels(gridSize, width, height) {

    for (let i = height + 1; i > height * -1; i -= gridSize * 2) {

        if (i <= radius) {
            setText((i / 200).toFixed(1).toString(), width + 10, (canvasHeight - i - height + 1));
        }
    }
}

function circleFormula(x, r) {    
    let y = Math.sqrt(r * r - x * x);    
    return y;
}

function drawArrowHeads(offH, offV, canvas, w, h) {

    (w == undefined) ? w = 10 : w;
    (h == undefined) ? h = 20 : h;

    let imageWidth = canvas.width;
    let imageHeight = canvas.height;
    let ctx = canvas.getContext('2d');

    ctx.fillStyle = '#666';
    ctx.translate(0.5, 0.5);

    setpixelated(ctx);

    ctx.beginPath();

    // X
    ctx.moveTo(h, offV + w);
    ctx.lineTo(0, offV);
    ctx.lineTo(h, offV - w);

    ctx.moveTo(imageWidth - h, offV - w);
    ctx.lineTo(imageWidth, offV);
    ctx.lineTo(imageWidth - h, offV + w);

    ctx.moveTo(offH - w, imageHeight - h);
    ctx.lineTo(offH, imageHeight);
    ctx.lineTo(offH + w, imageHeight - h);

    // Y
    ctx.moveTo(offH - w, h);
    ctx.lineTo(offH, 0);
    ctx.lineTo(offH + w, h);

    ctx.moveTo(offH - w, imageHeight - h);
    ctx.lineTo(offH, imageHeight);
    ctx.lineTo(offH + w, imageHeight - h);

    ctx.stroke();
}

// draw basic canvas circle
function basicCircle(ctx, x, y, radius, fill, stroke, strokeWidth) {
    ctx.beginPath()
    ctx.arc(x, y, radius, 0, 2 * Math.PI, false)
    if (fill) {
        ctx.fillStyle = fill
        ctx.fill()
    }
    if (stroke) {
        ctx.lineWidth = strokeWidth
        ctx.strokeStyle = stroke
        ctx.stroke()
    }
}

// https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
// http://rosettacode.org/wiki/Bitmap/Midpoint_circle_algorithm#C.23

// draws midpoint circle with no anti aliasing
function drawCircle(x0, y0, radius, canvas) {
    let x = radius;
    let y = 0;
    let decisionOver2 = 1 - x;   // Decision criterion divided by 2 evaluated at x=r, y=0
    let imageWidth = canvas.width;
    let imageHeight = canvas.height;
    let context = canvas.getContext('2d');
    let imageData = context.getImageData(0, 0, imageWidth, imageHeight);
    let pixelData = imageData.data;
    let makePixelIndexer = function (width) {
        return function (i, j) {
            let index = CHANNELS_PER_PIXEL * (j * width + i);
            //index points to the Red channel of pixel 
            //at column i and row j calculated from top left
            return index;
        };
    };
    let pixelIndexer = makePixelIndexer(imageWidth);
    let drawPixel = function (x, y) {
        let idx = pixelIndexer(x, y);
        pixelData[idx] = 255;	//red
        pixelData[idx + 1] = 0;	//green
        pixelData[idx + 2] = 0;//blue
        pixelData[idx + 3] = 255;//alpha
    };

    while (x >= y) {
        drawPixel(x + x0, y + y0);
        drawPixel(y + x0, x + y0);
        drawPixel(-x + x0, y + y0);
        drawPixel(-y + x0, x + y0);
        drawPixel(-x + x0, -y + y0);
        drawPixel(-y + x0, -x + y0);
        drawPixel(x + x0, -y + y0);
        drawPixel(y + x0, -x + y0);
        y++;
        if (decisionOver2 <= 0) {
            decisionOver2 += 2 * y + 1; // Change in decision criterion for y -> y+1
        } else {
            x--;
            decisionOver2 += 2 * (y - x) + 1; // Change for y -> y+1, x -> x-1
        }
    }

    context.putImageData(imageData, 0, 0);
}

function drawAxes() {
    DG.ctx.fillStyle = '#666';
    DG.ctx.fillRect(0, (canvasHeight / 2) - 1, canvasWidth, 1);
    DG.ctx.fillRect((canvasWidth / 2) - 1, 0, 1, canvasHeight);
}

function drawGrid(ctx) {

    //if (canvas.getContext) {

        for (let i = 0; i < DG.ui.gridHeight; i++) {

            for (let j = 0; j < DG.ui.gridWidth; j++) {
                ctx.fillStyle = '#FFF';
                ctx.fillRect(j * DG.ui.gridSize, i * DG.ui.gridSize, DG.ui.gridSize - 1, DG.ui.gridSize - 1);
            }
        }
    //}
}

function runGroups(){
    //console.log('runGroups() called');
    //finalArr = [];
    runCount = 0;

    DG.distLoopCount = 1;
    DG.loopCount = 0;

    const threshold = parseInt(document.getElementById('thresholdRange').value,10);
    const sampleRadius = DG.qtParams.qTreeSampleRadius;
    const qTree = DG.qTree;
    const pointArray = DG.pointArray;
    
    DG.rangeValElm.innerHTML = threshold;

    //console.log('runGroups threshold:',threshold);
    // getQuadtreeDist(distArr,qTree,qtParams,threshold)
    //console.clear();

    // console.time('getDist');
    // let distArrayOld = getDist(pointArray,threshold);
    // console.timeEnd('getDist');
    
    //console.warn('wha? this should be a lot faster than getDist() which just uses brute force');
    //console.time('getQuadtreeDist');
    let distArray = getQuadtreeDist(pointArray,qTree,sampleRadius,threshold);
    //console.timeEnd('getQuadtreeDist');
    //let distArray = getDist(DG.pointArray,DG.threshold);
    //console.clear();
    //let outputArr = [...getGroups(distArray)];

    // console.time('Mat: getGroups');
    // let outputArr = getGroups(distArray.near);
    // console.timeEnd('Mat: getGroups');

    //console.time('Keef: _getGroups');
    let outputArrK = _getGroups(distArray.near);
    //console.timeEnd('Keef: _getGroups');
    let farArr = distArray.far;
    let nearArr = distArray.near;

    //outputArr = [...outputArr,...farArr];
    outputArrK = [...outputArrK,...farArr];

    //outputArr = outputArr.concat(farArr);

    //console.clear();
    // console.log('distArray:', distArray);
    // console.log('nearArr:', nearArr);
    // console.log('farArr:', farArr);
    // console.log('outputArr:', outputArr);
    // console.log('keef outputArr:', outputArrK);
    
    
    //console.log('qTree:',DG.qTree);

    let groupCount = outputArrK.length;

    DG.groupsCounterElm.innerHTML = groupCount || 0;
    DG.pointsCounterElm.innerHTML = DG.pointArray.length || 0;

    // for (let i = 0; i < outputArr.length; i++) {
    //     let innerArr = outputArr[i];
    //     let currGroupArr = [];

    //     for (let o = 0; o < innerArr.length; o++) {
    //         currGroupArr.push(DG.pointArray[innerArr[o]]);
    //     }
    //     //console.log('currGroupArr:', currGroupArr);
    //     getBoundingBox(currGroupArr);
        
    // }
    // render keefs groups with a red border
    for (let i = 0; i < outputArrK.length; i++) {
        let innerArr = outputArrK[i];
        let currGroupArr = [];

        for (let o = 0; o < innerArr.length; o++) {
            currGroupArr.push(DG.pointArray[innerArr[o]]);
        }
        //console.log('currGroupArr:', currGroupArr);
        //getBoundingBox(currGroupArr,4,'red');
        getBoundingBox(currGroupArr);
        
    }

}

// extended quadtree with draw function to display the tree visually
// requires the Canvas 2d drawing context - ctx
function quadTreeDraw(ctx,qTree) {     
    ctx.lineWidth = 1;        
    let opacity = 1;
    ctx.strokeStyle = `rgba(255, 0, 0, ${opacity})`;        

    let left = qTree.bounds.x;        
    let top = qTree.bounds.y;
    let width = qTree.bounds.width;
    let height = qTree.bounds.height;

    ctx.strokeRect(left, top, width, height);
    
    // recursively draw child quadTrees
    if (qTree.nodes.length > 0) {
        quadTreeDraw(ctx,qTree.nodes[0]);
        quadTreeDraw(ctx,qTree.nodes[1]);
        quadTreeDraw(ctx,qTree.nodes[2]);
        quadTreeDraw(ctx,qTree.nodes[3]);        
    }
}

function logResults(){
    clear();
    distLoopCount = 0;
    finalArr = [];
    runCount = 0;
    var distanceArray = getDist(pointArray);
    var output = getGroups(distanceArray);
    
    console.log('pointArray: ', pointArray);
    console.log('distance Array:', distanceArray);
    console.log('distLoopCount: ', distLoopCount);
    console.log('output: ', output);
    console.log('runCount: ', runCount);
}

if (typeof window !== 'undefined') {
    window.addPoints = addPoints;
    window.getBoundingBox = getBoundingBox;
    window.update = update;
    window.Circle = Circle;
}


export {
    uPush,
    drawGrid,
    update,
    debounce,
    runGroups,
    getDistance,
    getDistanceSqr,
    quadTreeDraw,
    getHoverPoint,
    getHoverPointQuad,
    getBoundingBox,
    addRandomPoints,
    addPoints,
    movePoint,
    query
}
